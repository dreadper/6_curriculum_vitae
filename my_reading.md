# My reading 

A list of articles I read 

## Admin system

### Container

- explaining how to escape from container : https://pet2cattle.com/2022/01/container-escape

#### Podman 

- How podman work on mac os : https://www.redhat.com/sysadmin/podman-mac-machine-architecture
- Running rootless Podman as a non-root user : https://www.redhat.com/sysadmin/rootless-podman-makes-sense
- podman and user namespace : https://opensource.com/article/18/12/podman-and-user-namespaces

#### Docker 

- escaping docker privileged container : https://betterprogramming.pub/escaping-docker-privileged-containers-a7ae7d17f5a1
- docker vs podman : https://searchservervirtualization.techtarget.com/tip/Compare-Docker-vs-Podman-for-container-management

