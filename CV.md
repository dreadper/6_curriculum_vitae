# CV 

## Administration Réseau

### Switching 

- STP
- ARP

### Routage

- en cours

### Wifi :

- Arubas OS 6.x - 8.x,airwave
- Mist (Juniper)
- Normes 802.11
- Design Haute densité

## Administration Système

### OS

- Windows server 2008 - 2012
- client windows 7 - 10
- centos
- ubuntu 
- debian : 8 - 10
- Mac OS :  Yosemite - Monterey
- Raspberry : raspbian

### Virtualisation 

- Docker
- Podman
- virtualbox

### Services basiques 

- DNS : Bind9
- DoH : cloudflared
- Proxy : squid
- Base de donnée : Mysql, Microsoft SQL
- PRA et PCA

### Automatisation (devops)

- shell / bash
- Git : Github, gitlab Gitea
- Vagrant
- Ansible
- Netmiko
- API REST

### Supervision

- TIG : Telegraf, influx-db, Grafana

### Sauvegarde 

- Restic

### Log

- syslog-ng
- ELK (Elastic search, Logstash, Kibana)

### Authentification

- en cours

## CyberSécurité

- en cours

## Développement web

- Backend : Symfony : 2 - 4, Django, Flask, API REST
- Frontend : React.js
- CSS : Bootstrap, Bulma.io
- Test fonctionnel : Selenium 
- python webscraping : Selenium, beautiful soup


## Gestion de projet

- redmine